'use strict';

module.exports.envo = (event, context, callback) => {

    const response = {
        statusCode: 200,
        body: JSON.stringify({
          message: "this is key: " + process.env.OTHER_KEY
        }),
      };
    callback(null, response);
};